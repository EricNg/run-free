<?php require 'header.php'; ?>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="">
<meta name="author" content="">

<!-- Note there is no responsive meta tag here -->

<link rel="shortcut icon" href="../../docs-assets/ico/favicon.png">
<title>RUN 4 FREE</title>

<!-- Bootstrap core CSS -->
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/bootstrap-theme.css" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="css/non-responsive.css" rel="stylesheet">
<link href="css/main.css" rel="stylesheet">
<link href="css/navi.css" rel="stylesheet">
<link href="css/validationEngine.jquery.css" rel="stylesheet">

<!-- Font awesome icon -->
<link rel="stylesheet" href="css/font-awesome.css">

<!-- Just for debugging purposes. Don't actually copy this line! -->
<!--[if lt IE 9]><script src="../../docs-assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    
<style>
	.form-group {
		padding:3px 0;
	}
	
	.col-xs-5, .col-xs-10 {
		padding-left:0px;
		padding-top:10px;
		
	}


</style>    
</head>

<body>
<div class="container">
  <div id="fb-root"></div>
  <script type="text/javascript" src="js/main.js"></script> 
  
  <!-- Fixed navbar -->
   <nav id="navi">
   <ul id="appleNav" class="nav-justified">

                <li><a href="index.php">How To Participate</a></li>
                <li class="active"><a href="submit.php">Upload Your Photo To Participate</a></li>
                <li><a href="gallery.php">Voting & <br>Leaderboard</a></li>
                <li><a href="winners.php">Winners</a></li>
                <li><a href="about_us.php">About Us</a></li>
                <li><a href="tnc.php">Terms & Conditions</a></li>
               
            </ul>
  </nav>
  <div > <img src="images/sub_banner.jpg"> </div>
  <!-- /container -->
  
  <div class="content">
   <center>
      <h3 class="header-color">THIS CONTEST IS CLOSED</h3>
      <h3 class="header-color">WINNERS WILL BE ANNOUNCED ON 20 JANUARY 2014</h3>
      <h3 class="header-color">VISIT THE “WINNERS” PAGE TO FIND OUT IF YOU AND YOUR FRIENDS CAN RUN 4 FREE IN 2014!</h3>
      <p>&nbsp;</p>
       <p>&nbsp;</p>
        <p>&nbsp;</p>
         <p>&nbsp;</p>
          <p>&nbsp;</p>
           <p>&nbsp;</p>
      </center>
   
  </div>
 
  
   <!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
    <div class="modal-header" style="background:#000; color:#FFF">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span style="color:#FFF">&times;</span></button>
      </div>
    
      <div class="modal-body" style="background:#000; color:#FFF">
      	<div id="modal-body-response"></div>
        <h3>Terms and Conditions </h3>
      <ol>
        <li>The U Run Premium Package&nbsp;is not transferable and must only be  used by the winner. Any misuse of the package will result in the package being  cancelled. </li>
        <li>Any&nbsp;unauthorised&nbsp;registration for the U Run Premium  Package 2014 and related events will be removed and revoked without notice. </li>
        <li>The U Run Premium  Package 2014 membership will automatically be terminated from when the  registration period of the final event closes or on 31 Dec 2014, whichever is  earlier. </li>
        <li>All registration of  events&nbsp;utilising the U Run  Premium Package&nbsp;credit / credits  will be conducted online. </li>
        <li>U Run Premium Package  holders will receive a maximum of 4 run credits to utilise for 4 running events  from the&nbsp;NTUC Club (U Sports)&nbsp;list of participating events. </li>
        <li>No extension of the&nbsp;registration period&nbsp;will be given once it is fixed. </li>
        <li>You must be either be  a NTUC / nEbO Member in order to utilise the U Run Premium Package credits. </li>
        <li>Once registration is  confirmed,&nbsp;there will be no  refund of credit / credits, replacement with other events or change of&nbsp;race&nbsp;category/distances. </li>
        <li>U Run Premium Package credit / credits only valid for listed events (stated by NTUC Club U Sports) from Jan 2014 to Dec 2014, unless stated otherwise.</li>
        <li>Credit / credits can  only be used for races of full marathon distance and below. Ultra-marathons are  not included. </li>
        <li>Any unused credit /  credits will be forfeited and there will be&nbsp;no  refund&nbsp;in partial or in whole. </li>
        <li>In the event shall a  running event be cancelled, there will be no refund of credit / credits in  partial or in whole. NTUC Club (U Sports) shall not be liable for any other  loss or inconvenience caused. </li>
        <li>NTUC Club (U Sports)&nbsp;reserves the right to change the date  and registration period for events. </li>
        <li>NTUC Club (U Sports)&nbsp;reserves the right to change the&nbsp;list of participating running events  under U Run Premium Package. </li>
        <li>NTUC Club (U Sports)&nbsp;and its staff are not responsible for  any loss of property, injury or death sustained during the&nbsp;races and activities. </li>
        <li>NTUC Club (U Sports)&nbsp;reserves the right to change or amend  the terms and conditions without prior notice. </li>
        <li>The use of the U Sports  website signifies your agreement to these terms of use which may be updated  from time to time at the sole discretion of&nbsp;NTUC  Club (U Sports).&nbsp;Notice of any  changes to these terms of use is solely through posting changes to these terms  contained herein. </li>
        <li>All prizes are to be  claimed in accordance to what it is stated on the contest. Prizes cannot be&nbsp;exchanged&nbsp;for cash or a different product/race  of choice. </li>
        <li>In the event of a tie  in the contest,&nbsp;winner will be  selected based on whoever&nbsp;garnered  the most votes in the shortest time. </li>
        <li>Participants must be  Singaporean/PR and of a minimum age of 18 years as of 20 Jan 2014 to claim the  winning prizes. </li>
        <li>Winners shall be  bound to the terms and conditions of the respective races and activities. </li>
        <li>All prizes are to be  claimed by 30 Jan 2014. Any unclaimed prizes shall be awarded to the next  winner who has garnered the 2nd highest votes. </li>
        <li>Winner of the Grand  Prize must be agreeable to be featured in RUN Singapore magazine dressed in O+R  apparel. </li>
        <li>Winner of the Grand  Prize will be announced on 20 Jan 2014. Winner shall be notified via email,  mail and mobile. </li>
        <li>The contest is open  from 28 Nov 2013 – 15 Jan 2014. </li>
        <li>The decision of the  judges shall be final. </li>
      </ol>
  
      </div>
      
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
  
  
  <!-- Bootstrap core JavaScript
    ================================================== --> 
  <!-- Placed at the end of the document so the pages load faster --> 
  <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script> 
  <script src="js/ajaxfileupload.js"></script> 
  <script src="js/jquery.validationEngine.js"  type="text/javascript"></script>
  <script src="js/languages/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>
  <script src="js/bootstrap.min.js"></script> 
  <!-- Add fancyBox main JS and CSS files --> 
  <script type="text/javascript" src="source/jquery.fancybox.js?v=2.1.5"></script>
  <link rel="stylesheet" type="text/css" href="source/jquery.fancybox.css?v=2.1.5" media="screen" />
  <script>
  
  
		var $jq=jQuery.noConflict();
		
		$jq(function()
		{
			$jq(".various").fancybox({
				maxWidth	: 800,
				maxHeight	: 600,
				fitToView	: false,
				width		: '70%',
				height		: '70%',
				autoSize	: false,
				closeClick	: false,
				openEffect	: 'none',
				closeEffect	: 'none'
			});
			
			<!-- Upload of Photo -->
			
			$jq("#browse").click(function(e) 
			{
				e.preventDefault();
				$jq("#uploadfield").toggle();
			
			});
			
			//Through browse button
			$jq('#submit-file-button').click(function(e)
			{
				$jq.ajaxFileUpload
				(
					{
						cache:false,
						dataType:'json',
						fileElementId:'upload-file',
						url:'upload.php',
						secureuri:false,
						success:function(data){							 
						   if(data.return_msg)
						   {
							  $jq("#uploadfield").hide();
							  $jq("#uploadimg").show();
							  $jq('#uploadimg').css('background','url(upload/'+data.url+') no-repeat');
							  $jq('#photo').val(data.url);
							 
						   								 
						   }						
						}
					}
				);
			});		
			
			jQuery("#form1").validationEngine({
					ajaxFormValidation: true,
					ajaxFormValidationMethod: 'post',
                    onAjaxFormComplete: ajaxValidationCallback
           });
	
		});	
		
		
		
		 // Called once the server replies to the ajax form validation request
		function ajaxValidationCallback(status, form, json, options){
 
			if (status === true) {
				if (json.return_msg == "success") {
					top.location.href="https://apps.facebook.com/runforfree_contest/thankyou.php?uid="+json.userId;
				}
				
				if (json.return_msg == "nophoto") {
					alert("Please upload a photo");
				}
				
			}
		}
		
		function setImageURL(imgurl) {
		
			$jq.ajax
				(
					{
						cache:false,
						dataType:'json',
						type: "GET",
						url: "upload.php",
						data: "url="+imgurl,
						
						success:function(data){	
	 
						   if(data.return_msg == "success")
						   {
						   	  
							  $jq("#uploadfield").hide();
							  $jq("#uploadimg").show();
							  $jq('#uploadimg').css('background','url(upload/'+data.url+') no-repeat');
							  $jq('#photo').val(data.url);
							  
						   }						
						},
						error:function(xhr,text_status,error){ $jq('#result').text("error");}
					}
				);
			
			$jq.fancybox.close();
			
			
		}
		
	</script> 
    <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-41227951-3', 'utrix.com.sg');
  ga('send', 'pageview');

</script>
    
</div>


</body>
</html>
