
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="">
<meta name="author" content="">

<!-- Note there is no responsive meta tag here -->

<link rel="shortcut icon" href="../../docs-assets/ico/favicon.png">
<title>RUN 4 FREE</title>

<!-- Bootstrap core CSS -->
<link href="css/bootstrap.css" rel="stylesheet">


<!-- Custom styles for this template -->
<link href="css/non-responsive.css" rel="stylesheet">
<link href="css/main.css" rel="stylesheet">
<link href="css/navi.css" rel="stylesheet">

<!-- Font awesome icon -->
<link rel="stylesheet" href="style/font-awesome.css">


<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<div class="container">
  <div id="fb-root"></div>
  <script type="text/javascript" src="js/main.js"></script> 
  
  <!-- Fixed navbar -->
  <nav id="navi">
    <ul id="appleNav" class="nav-justified">

                
                <li><a href="index.php">How To Participate</a></li>
                <li ><a href="submit.php">Upload Your Photo To Participate</a></li>
                <li><a href="gallery.php">Voting & <br>Leaderboard</a></li>
                <li class="active"><a href="winners.php">Winners</a></li>
                <li><a href="about_us.php">About Us</a></li>
                <li><a href="tnc.php">Terms & Conditions</a></li>
                
                
               
            </ul>
  </nav>
  </nav>
  <div > <img src="images/sub_banner.jpg"> </div>
  <!-- /container -->
  
  <div class="content">
    

  <h4 class="header-color">&nbsp;</h4>
  <div class="row">
  
  

  <div class="col-sm-4 col-md-4">
   <h3 class="header-color">GRAND PRIZE </h3>

    <div class="thumbnail">
      <img src="../run-free/upload/KGFBEBi.jpg" alt=""> </a>
      <div class="caption">
        <h4>Silas Chong</h4>
       <p>Vote Count: <strong class="header-color">933</strong></p>
       <p><a href="details.php?page=&uid=NQ==" class="btn btn-primary" role="button" >Click to View</a></p>
      </div>
    </div>
  </div>
  

  

  <div class="col-sm-4 col-md-4">
  <h3 class="header-color">2nd PRIZE</h3>

    <div class="thumbnail">
      <img src="../run-free/upload/LXU34ZE.jpg" alt=""> </a>
      <div class="caption">
        <h4>Sebastian Wang</h4>
       <p>Vote Count: <strong class="header-color">627</strong></p>
       <p><a href="details.php?page=&uid=Ng==" class="btn btn-primary" role="button" >Click to View</a></p>
      </div>
    </div>
  </div>
  

  

  <div class="col-sm-4 col-md-4">
   <h3 class="header-color">3rd PRIZE</h3>

    <div class="thumbnail">
      <img src="../run-free/upload/LgQM15D.jpg" alt=""> </a>
      <div class="caption">
        <h4>chen han-wen</h4>
       <p>Vote Count: <strong class="header-color">619</strong></p>
       <p><a href="details.php?page=&uid=MTU=" class="btn btn-primary" role="button" >Click to View</a></p>
      </div>
    </div>
  </div>
  

  </div>

  

  </div>

  
  <!-- Bootstrap core JavaScript
    ================================================== --> 
  <!-- Placed at the end of the document so the pages load faster --> 
  <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script> 
  <script src="js/bootstrap.min.js"></script> 
  <script>
		var $jq=jQuery.noConflict();
  </script>
  <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-41227951-3', 'utrix.com.sg');
  ga('send', 'pageview');

</script>
</div>
</body>
</html>
