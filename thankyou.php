<?php require 'header.php'; ?>


<?php

require_once("func_select.php");

   $uid= base64_decode($_GET['uid']);
   
   //Get current record
   $db = db_connect();	
	$sql = "SELECT photoId, firstName, lastName, caption FROM users WHERE userId=". mysql_real_escape_string($uid);
	$results = mysql_query($sql);
	$row = mysql_fetch_row($results);
	
	$photoId = $row[0];
	$firstName = $row[1];
	$lastName = $row[2];
	$caption = $row[3];


?>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="">
<meta name="author" content="">

<!-- Note there is no responsive meta tag here -->

<link rel="shortcut icon" href="../../docs-assets/ico/favicon.png">
<title>RUN 4 FREE</title>

<!-- Bootstrap core CSS -->
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/bootstrap-theme.css" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="css/non-responsive.css" rel="stylesheet">
<link href="css/main.css" rel="stylesheet">
<link href="css/navi.css" rel="stylesheet">

<!-- Font awesome icon -->
<link rel="stylesheet" href="style/font-awesome.css">

<!-- Just for debugging purposes. Don't actually copy this line! -->
<!--[if lt IE 9]><script src="../../docs-assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<div class="container">
  <div id="fb-root"></div>
  <script>

  window.fbAsyncInit = function() {
    // init the FB JS SDK
    FB.init({
      appId      : '549094805164385',                        // App ID from the app dashboard
      channelUrl : '', // Channel file for x-domain comms
      status     : true,                                 // Check Facebook Login status
      xfbml      : true                                  // Look for social plugins on the page
    });

    // Additional initialization code such as adding Event Listeners goes here
	FB.Canvas.setSize();
	
	FB.getLoginStatus(function(response) {
  if (response.status === 'connected') {
    // the user is logged in and has authenticated your
    // app, and response.authResponse supplies
    // the user's ID, a valid access token, a signed
    // request, and the time the access token 
    // and signed request each expire
    var uid = response.authResponse.userID;
    var accessToken = response.authResponse.accessToken;
  } else if (response.status === 'not_authorized') {
    // the user is logged in to Facebook, 
    // but has not authenticated your app
		FB.login(function(response) {
	   if (response.authResponse) {
		 console.log('Welcome!  Fetching your information.... ');
		 FB.api('/me', function(response) {
		   console.log('Good to see you, ' + response.name + '.');
		 });
	   } else {
		 console.log('User cancelled login or did not fully authorize.');
	   }
	 	}, {scope: 'email,user_likes, publish_stream, user_photos'});
	  } else {
		// the user isn't logged in to Facebook.
	  }
 });

	 
	
 
	
	// Here we subscribe to the auth.authResponseChange JavaScript event. This event is fired
  // for any authentication related change, such as login, logout or session refresh. This means that
  // whenever someone who was previously logged out tries to log in again, the correct case below 
  // will be handled. 
  FB.Event.subscribe('auth.authResponseChange', function(response) {
    // Here we specify what we do with the response anytime this event occurs. 
    if (response.status === 'connected') {
      // The response object is returned with a status field that lets the app know the current
      // login status of the person. In this case, we're handling the situation where they 
      // have logged in to the app.
	  
	 FB.api('/me/feed', 'post', { message: 'Help me RUN 4 FREE! Vote for me at https://apps.facebook.com/runforfree_contest/details.php?uid=<?php echo $_GET['uid']?>', picture: 'https://www.utrix.com.sg/staging/<?php echo $photoId?>', link: 'https://apps.facebook.com/runforfree_contest/details.php?uid=<?php echo $_GET['uid']?>', name: 'RUN 4 FREE', description: 'Here is your chance to RUN 4 FREE marathons of your choice, plus other fabulous perks to spur you on!' },function (response){} );
	 
	 FB.ui({method: 'apprequests',
			message: 'Invite your friends to vote for you!',
			data: '{"uid":"<?php echo $_GET['uid']?>"}',
			title: 'Vote for me at RUN 4 FREE!'}); 				
	  
    } 
  });
  };
	


   (function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=549094805164385";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

   
   

</script> 
  
  <!-- Fixed navbar -->
  <nav id="navi">
   <ul id="appleNav" class="nav-justified">

               
                <li><a href="index.php">How To Participate</a></li>
                <li class="active"><a href="submit.php">Upload Your Photo To Participate</a></li>
                <li><a href="gallery.php">Voting & <br>Leaderboard</a></li>
                <li><a href="winners.php">Winners</a></li>
                <li><a href="about_us.php">About Us</a></li>
                <li><a href="tnc.php">Terms & Conditions</a></li>
               
               
            </ul>
  </nav>
  </nav>
  <div > <img src="images/sub_banner.jpg"> </div>
  <!-- /container -->
  
  <div class="content">
    <div class="span12">
      <h3 class="header-color">Thank you for participating in RUN 4 FREE 2014</h3>
    </div>
    <div class="span12 bulletstyle">Please <a href="gallery.php">click here</a> to view the gallery page. </div>
    <hr>
  </div>
  
  <!-- Bootstrap core JavaScript
    ================================================== --> 
  <!-- Placed at the end of the document so the pages load faster --> 
  <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script> 
  <script src="js/bootstrap.min.js"></script> 
  <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-41227951-3', 'utrix.com.sg');
  ga('send', 'pageview');

</script>
</div>
</body>
</html>
