<?php require 'header.php'; ?>



<?php 

if( isset($_REQUEST['request_ids']) ) {
 
// Requesting an application token
$token_url =    "https://graph.facebook.com/oauth/access_token?" .
"client_id=" . $facebook->getAppID() .
"&client_secret=" . $facebook->getApiSecret() .
"&grant_type=client_credentials";
$app_token = file_get_contents($token_url);
 
// You may have more than one request, so it's better to loop
$requests = explode(',',$_REQUEST['request_ids']);  
foreach($requests as $request_id) {
                    // Get the request details using Graph API
	$request_content = json_decode(file_get_contents("https://graph.facebook.com/$request_id?$app_token"), TRUE);
 
    // An example of how to get info from the previous call
    $from_id = $request_content['from']['id'];
    $to_id = $request_content['to']['id']; 
 
     // An easier way to extract info from the data field
     extract(json_decode($request_content['data'], TRUE));
     
	 // Now that we got the $item_id and the $item_type, process them
     // Or if the recevier is not yet a member, encourage him to claims his item (install your application)! 
     
	 
	 header("Location: details.php?uid=".$uid);
	 
	 //$redirect_from_app = $fbconfig['appUrl'] . "&app_data=" . $uid;
     //$loginUrl = $facebook->getLoginUrl(array('scope' => 'email,publish_stream', 'redirect_uri' => $redirect_from_app));
	 
     // When all is done, delete the requests because Facebook will not do it for you!
     //$deleted = file_get_contents("https://graph.facebook.com/$request_id?$app_token&method=delete"); // Should return true on success
                }
            }
?>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="">
<meta name="author" content="">

<!-- for Facebook -->          
<meta property="og:title" content="RUN 4 FREE" />
<meta property="og:image" content="https://www.utrix.com.sg/staging/run-free/images/banner.jpg" />
<meta property="og:url" content="https://apps.facebook.com/runforfree_contest/" />
<meta property="og:description" content="Here is your chance to RUN 4 FREE marathons of your choice, plus other fabulous perks to spur you on" />

<!-- Note there is no responsive meta tag here -->

<link rel="shortcut icon" href="../../docs-assets/ico/favicon.png">
<title>RUN 4 FREE</title>

<!-- Bootstrap core CSS -->
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/bootstrap-theme.css" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="css/non-responsive.css" rel="stylesheet">
<link href="css/main.css" rel="stylesheet">
<link href="css/navi.css" rel="stylesheet">

<!-- Font awesome icon -->
<link rel="stylesheet" href="style/font-awesome.css">
<?php
	if ($user_is_fan == false) { ?>
<script>

		if (document.referrer.indexOf("apps.facebook.com") > 0) {
			window.top.location.href = "<?php echo $pageTabURL ?>";
		}
		


</script>
<?php } ?>



<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<div class="container">
  <div id="fb-root"></div>
  <script type="text/javascript" src="js/main.js"></script> 
  
  <!-- Fixed navbar -->
  
  
  <nav id="navi">
 
   <ul id="appleNav" class="nav-justified">
    
                <li class="active"><a href="index.php">How To Participate</a></li>
                <li ><a href="submit.php">Upload Your Photo To Participate</a></li>
                <li><a href="gallery.php">Voting & <br>Leaderboard</a></li>
                <li><a href="winners.php">Winners</a></li>
                <li><a href="about_us.php">About Us</a></li>
                <li><a href="tnc.php">Terms & Conditions</a></li>
            
            </ul>
           
  </nav>
   
 
  <div > 
  <?php if (!$userlike) { ?>
	 <img src="images/banner_like.jpg"> 
  <?php	} else {?> 
	  <img src="images/banner.jpg"> 
	<?php } ?>  
    
   
    
  </div>
  <!-- /container -->
  
  <div class="content">

    <div class="col-xs-12"><br>
      	 <img src="images/tagline.jpg">
      
    </div>
    <div class="col-xs-12 bulletstyle">
      
        <div style="font-size:12px;">
        <br />
        *All entries to be submitted by 15 January 2014. Winners will be announced on 20 January 2014.
        
        </div>
    </div>
    <hr>
    <div class="col-xs-12 bulletstyle2">
      <h3 class="header-color">GRAND PRIZE <img src="images/tag.jpg"></h3>
      <ul>
        <li><sup>&Lambda;</sup> 4 run credits (list of runs include adidas King of Road, Sundown Marathon &amp; many more)</li>
        <li>4 run recovery sessions</li>
        <li>Customised training programme with Journey Fitness Company</li>
        <li>A chance to be featured in RUN Singapore magazine</li>
        <li>1 year free subscription of RUN Singapore magazine</li>
        <li>U Sports running singlet and RUN Singapore limited edition O+R running attire</li>
      </ul>
      <div style="font-size:12px; padding-left:20px;">
      *U Sports & Run Singapore reserve the right to amend the terms of sales of the package without prior notice. Other T&Cs apply. <br>
      <sup>&Lambda;</sup> Up to a full marathon per credit. Choice of runs must be from the U Sports' list of participating events. Event schedule subject to change. 
      </div>
    </div>
    <hr>
    <div class="col-xs-12 bulletstyle2">
      <h3 class="header-color">2nd PRIZE</h3>
      <ul>
        <li>Opportunity to run 1 free <sup>&Lambda;</sup>marathon in 2014</li>
        <li>1 set of limited edition O+R running attire</li>
        <li>1 year free subscription of RUN Singapore magazine</li>
      </ul>
    </div>
    <hr>
    <div class="col-xs-12 bulletstyle2">
      <h3 class="header-color">3rd PRIZE</h3>
      <ul>
        <li>Opportunity to run 1 free <sup>&Lambda;</sup>marathon in 2014</li>
        <li>1 year free subscription of RUN Singapore magazine</li>
      </ul>
    </div>

  
  </div>
  
  <!-- Bootstrap core JavaScript
    ================================================== --> 
  <!-- Placed at the end of the document so the pages load faster --> 
  <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script> 
  <script src="js/bootstrap.min.js"></script>
  <script>
		var $jq=jQuery.noConflict();
  </script>
  <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-41227951-3', 'utrix.com.sg');
  ga('send', 'pageview');

</script>
  
  
</div>
</body>
</html>
