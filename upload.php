<?php

include_once 'resize.image.class.php';


$arrResult=array();

$inputFile='upload-file';
$uploadFolder='upload/';

$url=(empty($_GET['url']) ? NULL : $_GET['url']);


$imageName=GenerateRandomAlphanumeric(7);

$imageExtension = "";

if (!empty($_FILES[$inputFile]['tmp_name'])|| $url!=NULL)
{
	if (!empty($_FILES[$inputFile]['tmp_name'])) // Browse Computer
	{
		if (!getimagesize($_FILES[$inputFile]['tmp_name']))
		{
			$arrResult['return_msg']='No size';
			
		} else {
			$filename=basename($_FILES[$inputFile]['name']);
			$imageExtension=strtolower(substr($filename,strrpos($filename,'.')));
			
			if (!move_uploaded_file($_FILES[$inputFile]['tmp_name'],$uploadFolder.$imageName.$imageExtension))
			{
				$arrResult['return_msg']='Fail to upload';
			} else {
				$arrResult['return_msg']='success';
				
			}
		}
	} else { // Through facebook Gallery
		if (!getimagesize($url))
		{
			$arrResult['return_msg']='Fail';
		} else {
			$imageExtension=strtolower(substr($url,strrpos($url,'.')));
			
			if ($image=file_get_contents($url))
			{
				if (!file_put_contents($uploadFolder.$imageName.$imageExtension,$image))
				{
					$arrResult['return_msg']='Fail URL 1';
					
				} else {
					$arrResult['return_msg']='success';
					$imageExtension;
				}
			} else {
				$arrResult['return_msg']='Fail URL 2';
				
			}
		}
	}
} else {
	$arrResult['return_msg']='No file received';

}



//Here we resize the image
$imageOriginal=$uploadFolder.$imageName.$imageExtension;
//$imageName=GenerateRandomAlphanumeric(7);

$info=getimagesize($imageOriginal);
$mime=image_type_to_mime_type($info[2]);

if ($mime=='image/jpeg'||
	$mime=='image/jpg'||
	$mime=='image/gif'||
	$mime=='image/png')
{
	$image=new Resize_Image;
	$image->new_width=600;
	//$image->new_height=384;
	$image->image_to_resize=$imageOriginal;
	$image->ratio=true;
	$image->new_image_name=$imageName;
	$image->save_folder=$uploadFolder;
	$process=$image->resize();
	
	if (!$process['result']&&
		$image->save_folder)
	{
		$arrResult['return_msg']="success";
	} else {
		$imageExtension=strtolower(substr($imageOriginal,strrpos($imageOriginal,'.')));
		
		$arrResult['return_msg']="success";
		$arrResult['url']=$imageName.$imageExtension;
	}
} else {
	$arrResult['return_msg']="Fail to get image";

}


echo json_encode($arrResult);


// Private functions
function GenerateRandomAlphanumeric($pintlength=6)
{
	$str='';
	$chars='123456789abcdefghijklmnpqrstuvwxyzABCDEFGHIJKLMNPQRSTUVWXYZ';
	$i=0;
	
	while ($i<$pintlength)
	{
		$str.=substr($chars,mt_rand(0,strlen($chars)-1),1);
		$i++;
	}
	
	return $str;
}

?>