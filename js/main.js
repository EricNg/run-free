

var accessToken;
var uid;

  window.fbAsyncInit = function() {
    // init the FB JS SDK
    FB.init({
      appId      : '549094805164385',                        // App ID from the app dashboards
      channelUrl : '', 									// Channel file for x-domain comms
      status     : true,                                 // Check Facebook Login status
      xfbml      : true,									// Look for social plugins on the page
	  cookie  	 : true,
	  oauth		 : true                          
    });

    // Additional initialization code such as adding Event Listeners goes here
	FB.Canvas.setSize();
	

	FB.getLoginStatus(function(response) {
  		if (response.status === 'connected') {
			// the user is logged in and has authenticated your
			// app, and response.authResponse supplies
			// the user's ID, a valid access token, a signed
			// request, and the time the access token 
			// and signed request each expire
			 uid = response.authResponse.userID;
			 accessToken = response.authResponse.accessToken;
			 

          var page_id = "238275896271759"; 
          var fql_query = "SELECT uid FROM page_fan WHERE page_id = "+page_id+"and uid="+uid;
          var the_query = FB.Data.query(fql_query);
		  
		  the_query.wait(function(rows) {

              if (rows.length == 1 && rows[0].uid == uid) {
                  $jq("#navi").show();

                  //here you could also do some ajax and get the content for a "liker" instead of simply showing a hidden div in the page.

              } else {
                  $jq("#navi").hide();
			
                  //and here you could get the content for a non liker in ajax...
              }
          });
			 

	  	} else if (response.status === 'not_authorized') {
			// the user is logged in to Facebook, 
			// but has not authenticated your app
			FB.login(function(response) {
	   			if (response.authResponse) {
					location.reload(true);
		 			console.log('Welcome!  Fetching your information.... ');
		 			FB.api('/me', function(response) {
		   			console.log('Good to see you, ' + response.name + '.');
				 	});
	   			} else {
					 console.log('User cancelled login or did not fully authorize.');
	   			}}, {scope: 'user_likes, publish_stream, user_photos'});
	  } else {
		// the user isn't logged in to Facebook.
	  }
 	});

	}; // end window load
  
  
(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=549094805164385";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
   
   
   