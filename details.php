
<?php 


require_once("func_select.php"); 


?>



<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="">
<meta name="author" content="">



<!-- Note there is no responsive meta tag here -->

<link rel="shortcut icon" href="../../docs-assets/ico/favicon.png">
<title>RUN 4 FREE</title>

<!-- Bootstrap core CSS -->
<link href="css/bootstrap.css" rel="stylesheet">


<!-- Custom styles for this template -->
<link href="css/non-responsive.css" rel="stylesheet">
<link href="css/main.css" rel="stylesheet">
<link href="css/navi.css" rel="stylesheet">

<!-- Font awesome icon -->
<link rel="stylesheet" href="css/font-awesome.css">



<!-- Just for debugging purposes. Don't actually copy this line! -->
<!--[if lt IE 9]><script src="../../docs-assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>


<body>
<div class="container">
  <div id="fb-root"></div>
  <script type="text/javascript" src="js/main.js"></script> 
  
  <!-- Fixed navbar -->
  <nav id="navi">
   <ul id="appleNav" class="nav-justified">
                 <li><a href="index.php">How To Participate</a></li>
                <li ><a href="submit.php">Upload Your Photo To Participate</a></li>
                <li class="active"><a href="gallery.php">Voting & <br>Leaderboard</a></li>
                <li><a href="winners.php">Winners</a></li>
                <li><a href="about_us.php">About Us</a></li>
                <li><a href="tnc.php">Terms & Conditions</a></li>
               
            </ul>
  </nav>
  </nav>
  </nav>
  <div > <img src="images/sub_banner.jpg"> </div>
  <!-- /container -->
  
  <div class="content">

   <?php 
   $uid= base64_decode($_GET['uid']);
   
   
   //Get current record
    $db = db_connect();	
	$sql = "SELECT photoId, firstName, lastName, caption FROM users WHERE userId=". mysql_real_escape_string($uid);
	$results = mysql_query($sql);
	$row = mysql_fetch_row($results);
	
	$photoId = $row[0];
	$firstName = $row[1];
	$lastName = $row[2];
	$caption = $row[3];
	
	
	$sql = "SELECT voteId FROM voters WHERE userId=". mysql_real_escape_string($uid);
	$results = mysql_query($sql);
	$votecount = mysql_num_rows($results);

	$pageNum = 1;

	// if $_GET['page'] defined, use it as page number
	if(isset($_GET['page']))
	{
		$pageNum = $_GET['page'];
	}
   
   ?>
   
   <div id="photodetail">
   <br><br>
       <img src="<?php echo "../".$photoId ?>" class="img-rounded">
  		
   
       <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-9">
            <h3>Submitted By: <?php echo $firstName?> <?php echo $lastName?> </h3>
            </div>
            
           
             <div class="col-xs-3">
             <br>
             
             
           <div class="fb-share-button pull-right" data-href="https://apps.facebook.com/runforfree_contest/details.php?uid=<?php echo $_GET['uid'] ?>" data-type="button_count"></div>
        </div> <!-- row -->
       </div>
        
       </div>
       
       
       
       <div class="col-xs-12">
        <h4>Vote Count: <span id="votecount"><?php echo $votecount ?></span> </h4>
       </div>
       
       <div class="col-xs-12">
        <?php echo htmlentities($caption) ?>
       </div>

       <div class="row" style="padding-left:10px; padding-top:10px;">
       <br>
       
       <div class="col-xs-7">
       	
       
       <!-- <button class="btn btn-orange" id="invitefriend">Invite your friends to vote!</button> -->
       </div>
       
    
       </div>
       
       <br />
          
       <div class="col-xs-12">
       <a href="gallery.php?page=<?php echo $pageNum?>">Back to Gallery</a>
   		</div>
        
        <div class="col-xs-12" >
        	<div class="fb-comments" data-href="https://apps.facebook.com/runforfree_contest/details.php?uid=<?php echo $_GET['uid'] ?>" data-numposts="10" data-width="580" data-colorscheme="dark"></div>
        
        </div>
        
        
   </div>
   
  </div>
  
  
  <!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="background:#000; color:#FFF">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span style="color:#FFF">&times;</span></button>
      </div>
      <div class="modal-body" style="background:#000; color:#FFF">
      	<div id="modal-body-response"></div>
        
        <a href="https://apps.facebook.com/runforfree_contest/about_us.php" target="_top"><img src="images/ad_banner.jpg"></a>
      </div>
      
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="background:#000; color:#FFF">
        
      </div>
      <div class="modal-body" style="background:#000; color:#FFF">
      	<div id="modal-body-response"></div>
        <center>
        <h3 class="header-color">Click on the LIKE US button below to start voting!</h3> 
        <div class="fb-like-box" data-href="https://www.facebook.com/RUNSingapore" data-colorscheme="dark" data-show-faces="true" data-header="true" data-stream="false" data-show-border="true"></div>
      	</center>
      </div>
      
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
  
  
  <!-- Bootstrap core JavaScript
    ================================================== --> 
  <!-- Placed at the end of the document so the pages load faster  --> 
  <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
  <script src="js/bootstrap.min.js"></script> 
  <script>
		var $jq=jQuery.noConflict();
		
		$jq(window).load(function(){
		
			
			var login  = false;
			var length = "0";
			
			CheckLogin();
			
						
			$jq('#vote').click(function(e)
			{
				CheckLogin();
				
				
				
				if (login == true) {

					$jq.ajax
						({
								cache:false,
								dataType:'json',
								type: "POST",
								url: "addvote.php",
								data: "t=<?php echo $token?>&uid=<?php echo $uid ?>&userId="+uid,
								success:function(data){ ResponseCallBack(data)},
								error:function(xhr,text_status,error){ $jq('#result').text("error");}
						});
				}
					
			}) //end vote button
			
			
			$jq('#invitefriend').click(function(e)
			{
				CheckLogin();
				
				if (login == true) {
					FB.ui({method: 'apprequests',
					message: 'Invite your friends to vote for <?php echo $firstName?> <?php echo $lastName?>!',
					data: '{"uid":"<?php echo $_GET['uid']?>"}',
					title: 'Help vote for <?php echo $firstName?> <?php echo $lastName?> at RUN 4 FREE!'}); 			
				}
					
			}) //end invitefriend
			
			
			function getParameterByName(name) {
				name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
				var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
					results = regex.exec(location.search);
				return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
			}
			
			ResponseCallBack(getParameterByName("s"));
			
			function CheckLogin() {
				
				FB.getLoginStatus(function(response) {
					if (response.status === 'connected') {
						// the user is logged in and has authenticated your
						// app, and response.authResponse supplies
						// the user's ID, a valid access token, a signed
						// request, and the time the access token 
						// and signed request each expire
						uid = response.authResponse.userID;
						$jq("#userId").val(uid);
						var accessToken = response.authResponse.accessToken;
						login = true;
						
						 FB.api('/me/likes/238275896271759', function(response) {
							if (response.data.length == 0)
							{
								$jq('#myModal2').modal('show'); 		
							}

						  });

						
						  FB.Event.subscribe('edge.create',
							function(href, widget) {
								$jq('#myModal2').modal('hide')
							}
						);
						
					} else if (response.status === 'not_authorized') {
						// the user is logged in to Facebook, 
						// but has not authenticated your app
						FB.login(function(response) {
							if (response.authResponse) {
								console.log('Welcome!  Fetching your information.... ');
								FB.api('/me', function(response) {
								console.log('Good to see you, ' + response.name + '.');
								});
							} else {
								 console.log('User cancelled login or did not fully authorize.');
							}}, {scope: 'user_likes, publish_stream, user_photos'});
				  } else {
					// the user isn't logged in to Facebook.
				  }
				});	
			}
			
			
			
			function ResponseCallBack(s) {
				if(s == "true")
				{
	
					FB.ui({
						method: 'feed',
						name: 'I have voted for <?php echo $firstName?> <?php echo $lastName?>\'s entry in the RUN 4 FREE contest.',				
						link:'https://apps.facebook.com/runforfree_contest/details.php?uid=<?php echo $_GET['uid'] ?>',
						caption: ' Vote for <?php echo $firstName?> <?php echo $lastName?> daily and get  <?php echo $firstName?> <?php echo $lastName?> to the top of the leader board! ',
						picture: 'https://www.utrix.com.sg/staging/run-free/upload/<?php echo $photoId ?>'					 
					});
					
					
					$jq("#modal-body-response").html("<center><h3 class='header-color'>THANK YOU FOR VOTING</h3><center>");
					$jq('#myModal').modal();
					
					
				}
				
				if(s == "false")
				{
		
					$jq("#modal-body-response").html("<center><h3 class='header-color'>YOU CAN VOTE FOR EACH ENTRY ONLY ONCE DAILY</h3></center>");
					$jq('#myModal').modal();
				}			  
			}
			
			
		}); //end function
  
  </script>
  <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-41227951-3', 'utrix.com.sg');
  ga('send', 'pageview');

</script>
</div>
</body>
</html>
