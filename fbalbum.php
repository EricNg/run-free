<?php
require 'src/facebook.php';

// Create our Application instance (replace this with your appId and secret).
$facebook = new Facebook(array(
  'appId'  => '549094805164385',
  'secret' => 'eda742a347df8f73c469040e25fad79f',
));


// Get User ID
$user = $facebook->getUser();
$access_token = $_REQUEST["signed_request"];


?>
<!DOCTYPE html>
<html lang="en" xmlns:fb="http://www.facebook.com/2008/fbml">

<head>

	<link href="css/bootstrap.css" rel="stylesheet">


	<script src="js/jquery-1.9.1.min.js"></script>
	<script>
	var $jq=jQuery.noConflict();
	
	var user_id="<?php echo((isset($user)&&$user!=null) ? $user : ""); ?>";
	var access_token="<?php echo $access_token; ?>";
	</script>
    
	
  <style>
  
	/*=====================
	photo album  Style
	======================= */
	
	 
.header-color {
	color:#F36F21;
	font-weight:bold;
	text-transform:uppercase;
}
	
	body {
		background-color:#000;
		padding:0px;
	}
	
	#album_wrapper ul {
		padding:0;
		margin:0;
	}
	
	#photo_wrapper ul {
		padding:0;
		margin:0;
	}
	
	#album_wrapper ul li {
		list-style:none;
		float:left;
		padding:5px;

		
	}
	
	#photo_wrapper  ul li {
		list-style:none;
		float:left;
		padding:5px;
		width:150px;
		height:113px;
		
	}
	
	.album {
		width:147;
		height:111;
	}
  
  
  </style>
  
</head>

<body>
	
	<div id="fb-root"></div>
	<script src="https://connect.facebook.net/en_US/all.js"></script>
	<script>
	$jq(function()
	{
		
		var $wrapper=$jq('#retrieve_wrapper');
		
		FB.api('/me/albums',{access_token:access_token},function(response)
		{
			// check for a valid response
			if (!response || response.error)
			{
				return;
			}
			
			getAlbums(response.data);
		
		});
	});
	
	
	function goToByScroll(id){
     	$jq('html,body').animate({scrollTop: $jq("#"+id).offset().top},'slow');
	}

	
	// Ref: http://stackoverflow.com/questions/5276088/how-do-i-get-album-pictures-using-facebook-api
	function getAlbums(album_json)
	{
		var $wrapper=$jq('#album_wrapper');
		var html='<ul>';
		
		for (var i=0,l=album_json.length;i<l;i++)
		{
			var aid=album_json[i].id;
			
			html+='<li>'+
			
			'<div aid="'+aid+'" class="album" style="background-image:url(https://graph.facebook.com/'+aid+'/picture?access_token='+access_token+'); width:147px; height:111px; cursor:pointer">' +
					
						'</div>'+
					'</li>';
		}
		
		html+='</ul>';
		$wrapper.html(html);
		
		$jq('.album',$wrapper).click(function(e)
		{	
			e.preventDefault();
			
			getPhotos($jq(this).attr('aid'));
		});
	}
	
	
	function getPhotos(aid)
	{
		var $wrapper=$jq('#photo_wrapper');
		
		var html="<h4 class='header-color'>Select A Photo</h4>";
		html +='<ul>';
		
		FB.api('/'+aid+'/photos',{access_token:access_token},function(response)
		{
			// check for a valid response
			if (!response || response.error)
			{
				alert("error occured");
				return;
			}
			
			var photosJson=response.data;
			for (var i=0,l=photosJson.length;i<l;i++)
			{
				var photo=photosJson[i].picture;
				var source=photosJson[i].source;
				
				html+='<li>'+
							'<a href="#" source="'+source+'" class="photo">'+
								'<img src="'+photo+'" alt="" />'+
							'</a>'+
						'</li>';
			}
			
			html+='</ul>';
			$wrapper.html(html);
			
			goToByScroll("photos");
			
			
			$jq('.photo',$wrapper).click(function(e)
			{
				e.preventDefault();
				
				uploadPhoto($jq(this).attr('source'));
			});
		});
		
		
	}
	
	function uploadPhoto(source)
	{
		//alert(source);
		parent.setImageURL(source);
	}
	</script>
	
	<div id="retrieve_wrapper">
	
		
        <h4 class='header-color'>Select an album</h4>
        <a name="album" id="album"></a>
		<div id="album_wrapper">	
		</div>
		<div style="clear:both"></div>
        
       <br />
        
        <a name="photos" id="photos"></a>
		<div id="photo_wrapper">
		</div>
           <div style="clear:both"></div>

            
        
	</div>
	
</body>
</html>