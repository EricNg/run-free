<?php require 'header.php'; ?>


<?php require_once("func_select.php"); 
 $db = db_connect();		

?>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="">
<meta name="author" content="">

<!-- Note there is no responsive meta tag here -->

<link rel="shortcut icon" href="../../docs-assets/ico/favicon.png">
<title>RUN 4 FREE</title>

<!-- Bootstrap core CSS -->
<link href="css/bootstrap.css" rel="stylesheet">


<!-- Custom styles for this template -->
<link href="css/non-responsive.css" rel="stylesheet">
<link href="css/main.css" rel="stylesheet">
<link href="css/navi.css" rel="stylesheet">

<!-- Font awesome icon -->
<link rel="stylesheet" href="css/font-awesome.css">
 <link href="css/ticker-style.css" rel="stylesheet" type="text/css" />

<!-- Just for debugging purposes. Don't actually copy this line! -->
<!--[if lt IE 9]><script src="../../docs-assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<div class="container">
  <div id="fb-root"></div>
  <script type="text/javascript" src="js/main.js"></script> 
  
  <!-- Fixed navbar -->
  <nav id="navi">
  <ul id="appleNav" class="nav-justified">

                
                 <li><a href="index.php">How To Participate</a></li>
                <li><a href="submit.php">Upload Your Photo To Participate</a></li>
                <li class="active"><a href="gallery.php">Voting & <br>Leaderboard</a></li>
                <li><a href="winners.php">Winners</a></li>
                <li><a href="about_us.php">About Us</a></li>
                <li><a href="tnc.php">Terms & Conditions</a></li>
               
            </ul>
  </nav>
  </nav>
  <div > <img src="images/sub_banner.jpg"> </div>
  <!-- /container -->
  
  <div class="content">
  
    <?php 
		// how many rows to show per page
		$rowsPerPage = 6;
		// by default we show first page
		$pageNum = 1;
		
		// if $_GET['page'] defined, use it as page number
		if(isset($_GET['page']))
		{
			$pageNum = $_GET['page'];
		}
	
  ?>

	<ul id="js-news" class="js-hidden">
		<li class="news-item">THIS CONTEST IS CLOSED</li>
        <li class="news-item">WINNERS WILL BE ANNOUNCED ON 20 JANUARY 2014</li>
        <li class="news-item">VISIT THE “WINNERS” PAGE TO FIND OUT IF YOU AND YOUR FRIENDS CAN RUN 4 FREE IN 2014!</li>
	</ul>
   

  <h4 class="header-color">&nbsp;</h4>
  <div class="row">
  <?php


// counting the offset
$offset = ($pageNum - 1) * $rowsPerPage;

$query = " SELECT userId, photoId, firstName, lastName FROM users where userId!=51";
$query .= " ORDER BY datetime_submitted DESC";
$query .= " LIMIT $offset, $rowsPerPage ";
		 
 
$result = mysql_query($query);

while($row = mysql_fetch_array($result))
{
	$sql = mysql_query("SELECT COUNT(voteId) AS votercount FROM voters WHERE userId=". $row["userId"]);
	$data=mysql_fetch_assoc($sql);
	$votecount =  $data['votercount'];
	
	
  ?>

  

  <div class="col-sm-4 col-md-4">
    <div class="thumbnail">
      <img src="<?php echo "../".$row["photoId"] ?>" alt=""> </a>
      <div class="caption">
        <h4><?php echo $row["firstName"] ?> <?php echo $row["lastName"] ?></h4>
       <p>Vote Count: <strong class="header-color"><?php echo $votecount ?></strong></p>
       <p><a href="details.php?page=<?php echo $pageNum?>&uid=<?php echo base64_encode($row["userId"])?>" class="btn btn-primary" role="button" >Click to View</a></p>
      </div>
    </div>
  </div>
  
<?php } ?>

  </div>

 <?php 
 	// how many rows we have in database

	$query = " SELECT COUNT(userId) AS numrows FROM users where userId!=51";
	$query .= " ORDER BY userId DESC";
	
	$result  = mysql_query($query);
	$row     = mysql_fetch_array($result);
	$numrows = $row['numrows'];
	// how many pages we have when using paging?
	$maxPage = ceil($numrows/$rowsPerPage);
 
 ?>
  
  <ul class="pagination">
  <?php for ($i = 1; $i <= $maxPage; $i++) { 
  	if ($pageNum == $i) {
  ?>
  	<li class="active"><a href="gallery.php?page=<?php echo $i ?>" ><?php echo $i ?></a></li>
  <?php } else { ?>
  	<li><a href="gallery.php?page=<?php echo $i ?>"><?php echo $i ?></a></li>
  
  <?php } 
  }
  ?>
  </ul>
  </div>
  
  <!-- Bootstrap core JavaScript
    ================================================== --> 
  <!-- Placed at the end of the document so the pages load faster --> 
  <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script> 
  <script src="js/bootstrap.min.js"></script> 
  <script src="js/jquery.ticker.js" type="text/javascript"></script>
  <script>
		$(document).ready(function ($) {
				$('#js-news').ticker();
		})
	
	</script>
  <script>
		var $jq=jQuery.noConflict();
  </script>
  <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-41227951-3', 'utrix.com.sg');
  ga('send', 'pageview');

</script>
</div>
</body>
</html>
